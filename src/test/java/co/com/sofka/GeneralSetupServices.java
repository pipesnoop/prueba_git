package co.com.sofka;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;

import java.util.HashMap;

public class GeneralSetupServices {

    protected final Actor actor = new Actor("Ivan");

    protected void actorCan (String URL_BASE) {
        actor.can(CallAnApi.at(URL_BASE));
    }


}
