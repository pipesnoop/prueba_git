package co.com.sofka;


public class SetupRest extends GeneralSetupServices {
    protected static final String URL_BASE_PARCHE = "https://parches-qa2.herokuapp.com";
    protected static final String RESOURCE_REGISTRO = "/crearUsuario";
    protected static final String RESOURCE_INICIO_SESION = "/inicioSesion/%s";
    protected static final String RESOURCE_CREAR_COMENTARIO = "/crearComentario";
    protected static final String RESOURCE_ELIMINAR_COMENTARIO = "/eliminar/{id}";
    protected static final String RESOURCE_ACTUALIZACION_USUARIO = "/actualizarUsuario";
    protected static final String CREAR_PARCHE = "/parches/crear";
    protected static final String ELIMINAR_PARCHE = "/parches/eliminar/";
    protected static final String DESHABILITAR_PARCHE = "/parches/deshabilitar";
    protected static final String EDITAR_PARCHE = "/parches/editar";
    protected static final String RESOURCE_MIS_PARCHES = "grupo2/misParches";
    protected static final String RESOURCE_DETALLE_PARCHES = "detalle-parche/61fc5540e67b3835150b04e6/VwJyagdrNBNaLUGqWLFdAooCyg83";
    protected static final String RESOURCE_LISTA = "/parches";


    protected void setupRest () {
        actorCan(URL_BASE_PARCHE);
    }
   

}

