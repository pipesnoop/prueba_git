package co.com.sofka.stepdefinitions.ui.layout;

import co.com.sofka.GeneralSetupUi;
import com.github.javafaker.Faker;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;

import java.util.Locale;

import static co.com.sofka.question.ui.LayoutPrivadoCerrarSesionQuestion.layoutPrivadoCerrarSesionQuestion;
import static co.com.sofka.question.ui.LayoutPrivadoInicioQuestion.layoutPrivadoInicioQuestion;
import static co.com.sofka.question.ui.LayoutPrivadoMasQuestion.layoutPrivadoMasQuestion;
import static co.com.sofka.question.ui.LayoutPrivadoMiPerfilQuestion.layoutPrivadoMiPerfilQuestion;
import static co.com.sofka.question.ui.LayoutPrivadoMisParchesQuestion.layoutPrivadoMisParchesQuestion;
import static co.com.sofka.question.ui.LayoutPrivadoParchesQuestion.layoutPrivadoParchesQuestion;
import static co.com.sofka.task.ui.OpenLandingPage.openLandingPage;
import static co.com.sofka.task.ui.layout.LayoutPrivadoCerrarSesion.layoutPrivadoCerrarSesion;
import static co.com.sofka.task.ui.layout.LayoutPrivadoInicio.layoutPrivadoInicio;
import static co.com.sofka.task.ui.layout.LayoutPrivadoLogin.layoutPrivadoLogin;
import static co.com.sofka.task.ui.layout.LayoutPrivadoMas.layoutPrivadoMas;
import static co.com.sofka.task.ui.layout.LayoutPrivadoMiPerfil.layoutPrivadoMiPerfil;
import static co.com.sofka.task.ui.layout.LayoutPrivadoMisParches.layoutPrivadoMisParches;
import static co.com.sofka.task.ui.layout.LayoutPrivadoParches.layoutPrivadoParches;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class LayOutPrivadoStepdefinition extends GeneralSetupUi {
    private static final Logger LOGGER = Logger.getLogger(LayOutPrivadoStepdefinition.class);

    // Escenario 1
    @Dado("que el usuario se encuentra en el layout privado")
    public void queElUsuarioSeEncuentraEnElLayoutPrivado() {
        try {
        actorSetupTheBrowser(actor.getName());
        theActorInTheSpotlight().wasAbleTo(
                openLandingPage()
        );
        }catch (Exception e){
            LOGGER.info("Error en la apertura de la pagina parches");
        }
    }

    @Cuando("el usuario hace click en el boton Mas")
    public void elUsuarioHaceClickEnElBotonCrearParche() {
        try {
        theActorInTheSpotlight().attemptsTo(
                layoutPrivadoLogin()
                        .withName(name+""+lastName)
                        .withEmail(email)
                        .andPassword(password),
                layoutPrivadoMas()
        );
        }catch (Exception e){
            LOGGER.info("Error en la creación de la cuenta");
        }

    }

    @Entonces("el usuario sera redireccionado a los servicios de la opcion del Mas {string}")
    public void elUsuarioSeraRedireccionadoALosServiciosDeLaOpcionDelMas(String ValidacionMas) {
        try {
        theActorInTheSpotlight().should(
                seeThat(
                        layoutPrivadoMasQuestion(), equalTo(ValidacionMas)
                )
        );
        }catch (Exception e){
            LOGGER.info("Error en la validacion del del boton mas");
        }
    }

    // Escenario 2
    @Dado("que el cliente ha ingresado en el layout privado")
    public void queElClienteHaIngresadoEnElLayoutPrivado() {
        try {
        actorSetupTheBrowser(actor.getName());
        theActorInTheSpotlight().wasAbleTo(
                openLandingPage()
        );
        }catch (Exception e){
            LOGGER.info("Error en la apertura de la pagina parches");
        }
    }

    @Cuando("el cliente hace click en el boton Mis parches")
    public void elClienteHaceClickEnElBotonMisParches() {
        try {
        theActorInTheSpotlight().attemptsTo(
                layoutPrivadoLogin()
                        .withName(name+""+lastName)
                        .withEmail(email)
                        .andPassword(password),
                layoutPrivadoMisParches()
        );
        }catch (Exception e){
            LOGGER.info("Error en la creación de la cuenta");
        }
    }

    @Entonces("el cliente sera redireccionado a los servicios de la opcion Mis parches y vera {string}")
    public void elClienteSeraRedireccionadoALosServiciosDeLaOpcionMisParchesYVera(String ValidacionMisParches) {
        try {
        theActorInTheSpotlight().should(
                seeThat(
                        layoutPrivadoMisParchesQuestion(), equalTo(ValidacionMisParches)
                )
        );
        }catch (Exception e){
            LOGGER.info("Error en la validacion del boton mis parches");
        }
    }


    // Escenario 3
    @Dado("que el consumidor ha ingresado en el layout privado")
    public void queElConsumidorHaIngresadoEnElLayoutPrivado() {
        try {
        actorSetupTheBrowser(actor.getName());
        theActorInTheSpotlight().wasAbleTo(
                openLandingPage()
        );
        }catch (Exception e){
            LOGGER.info("Error en la apertura de la pagina parches");
        }
    }

    @Cuando("el consumidor oprime el boton Parches")
    public void elConsumidorOprimeElBotonParches() {
        try {
        theActorInTheSpotlight().attemptsTo(
                layoutPrivadoLogin()
                        .withName(name+""+lastName)
                        .withEmail(email)
                        .andPassword(password),
                layoutPrivadoParches()
        );
        }catch (Exception e){
            LOGGER.info("Error en la creación de la cuenta");
        }
    }

    @Entonces("el consumidor es redireccionado a los servicios de la opcion y vera {string}")
    public void elConsumidorEsRedireccionadoALosServiciosDeLaOpcionYVera(String ValidacionParches) {
        try {
        theActorInTheSpotlight().should(
                seeThat(
                        layoutPrivadoParchesQuestion(), equalTo(ValidacionParches)
                )
        );
        }catch (Exception e){
            LOGGER.info("Error en la validacion del boton parches");
        }
    }

    // Escenario 4

    @Dado("que el cliente ha iniciado sesion en la pagina de el Parche")
    public void queElClienteHaIniciadoSesionEnLaPaginaDeElParche() {
        try {
        actorSetupTheBrowser(actor.getName());
        theActorInTheSpotlight().wasAbleTo(
                openLandingPage()
        );
        }catch (Exception e){
            LOGGER.info("Error en la apertura de la pagina parches");
        }
    }

    @Cuando("el cliente hace click en el boton Perfil")
    public void elClienteHaceClickEnElBotonPerfil() {
        try {
        theActorInTheSpotlight().attemptsTo(
                layoutPrivadoLogin()
                        .withName(name+""+lastName)
                        .withEmail(email)
                        .andPassword(password),
                layoutPrivadoMiPerfil()
        );
        }catch (Exception e){
            LOGGER.info("Error en la creación de la cuenta");
        }
    }

    @Entonces("el cliente sera redireccionado a los servicios de la opcion Perfil y vera {string}")
    public void elClienteSeraRedireccionadoALosServiciosDeLaOpcionPerfilYVera(String ValidacionMiPerfil) {
        try {
        theActorInTheSpotlight().should(
                seeThat(
                        layoutPrivadoMiPerfilQuestion(), equalTo(ValidacionMiPerfil)
                )
        );
        }catch (Exception e){
            LOGGER.info("Error en la validacion del boton Mi perfil");
        }
    }

    // Escenario 5
    @Dado("que el consumidor ha realizado un inicio de sesion en la pagina de el Parche")
    public void queElConsumidorHaRealizadoUnInicioDeSesionEnLaPaginaDeElParche() {
        try {
        actorSetupTheBrowser(actor.getName());
        theActorInTheSpotlight().wasAbleTo(
                openLandingPage()
        );
        }catch (Exception e){
            LOGGER.info("Error en la apertura de la pagina parches");
        }
    }

    @Cuando("el consumidor hace click en el boton Inicio")
    public void elConsumidorHaceClickEnElBotonInicio() {
        try {
        theActorInTheSpotlight().attemptsTo(
                layoutPrivadoLogin()
                        .withName(name+""+lastName)
                        .withEmail(email)
                        .andPassword(password),
                layoutPrivadoInicio()
        );
        }catch (Exception e){
            LOGGER.info("Error en la creación de la cuenta");
        }
    }

    @Entonces("el cliente sera redireccionado a los servicios de la opcion Inicio y vera {string}")
    public void elClienteSeraRedireccionadoALosServiciosDeLaOpcionInicioYVera(String ValidacionInicio) {
        try {
        theActorInTheSpotlight().should(
                seeThat(
                        layoutPrivadoInicioQuestion(), equalTo(ValidacionInicio)
                )
        );
        }catch (Exception e){
            LOGGER.info("Error en la validacion del boton Home");
        }
    }

    // Escenario 6
    @Dado("que el usuario ha realizado un inicio de sesion en la pagina de el Parche")
    public void queElUsuarioHaRealizadoUnInicioDeSesionEnLaPaginaDeElParche() {
        try {
        actorSetupTheBrowser(actor.getName());
        theActorInTheSpotlight().wasAbleTo(
                openLandingPage()
        );
        }catch (Exception e){
            LOGGER.info("Error en la apertura de la pagina parches");
        }
    }

    @Cuando("el usuario hace click en el boton Cerrar sesion")
    public void elUsuarioHaceClickEnElBotonCerrarSesion() {
        try {
        theActorInTheSpotlight().attemptsTo(
                layoutPrivadoLogin()
                        .withName(name+""+lastName)
                        .withEmail(email)
                        .andPassword(password),
                layoutPrivadoCerrarSesion()
        );
        }catch (Exception e){
            LOGGER.info("Error en la creación de la cuenta");
        }
    }

    @Entonces("el usuario sera redireccionado a los servicios de la opcion Cerrar sesion y vera {string}")
    public void elUsuarioSeraRedireccionadoALosServiciosDeLaOpcionCerrarSesionYVeraUsted(String ValidacionCerrarSesion) {
        try {
        theActorInTheSpotlight().should(
                seeThat(
                        layoutPrivadoCerrarSesionQuestion(), equalTo(ValidacionCerrarSesion)
                )
        );
        }catch (Exception e){
            LOGGER.info("Error en la validacion del boton cerrar sesion");
        }
    }

    Faker usFaker = new Faker(new Locale("en-US"));
    String name = usFaker.name().firstName();
    String lastName = usFaker.name().lastName();
    String email = name + lastName + "@gmail.com";
    String password = usFaker.bothify("Qa????@####");


}
