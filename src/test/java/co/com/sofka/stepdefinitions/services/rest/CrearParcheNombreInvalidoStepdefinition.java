package co.com.sofka.stepdefinitions.services.rest;

import co.com.sofka.SetupRest;
import co.com.sofka.model.services.rest.Parche;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Entonces;
import org.apache.http.HttpStatus;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;


import static co.com.sofka.task.services.rest.DoPost.doPost;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;


public class CrearParcheNombreInvalidoStepdefinition extends SetupRest {
    private final Parche parche = Parche.generarParche();
    private static final Logger logger = LogManager.getLogger(CrearParcheNombreInvalidoStepdefinition.class);

    @Cuando("el usuario ejecute la peticion con nombre del parche {string}")
    public void elUsuarioEjecuteLaPeticionConNombreDelParche(String nombreParche) {
        try {
            parche.setNombre(nombreParche);
            super.setupRest();
            actor.attemptsTo(
                    doPost()
                            .withTheResource(CREAR_PARCHE)
                            .andTheBodyRequest(parche.parcheRequest())
            );
        } catch (Exception e) {
            logger.warn("error al ejecutar petición\n"+e);
            Assertions.fail(e.getMessage());
        }
    }

    @Entonces("se debe lanzar un error por la cantida de caracteres en el nombre del parche")
    public void seDebeLanzarUnErrorPorLaCantidaDeCaracteresEnElNombreDelParche() {
        try {
            actor.should(
                    seeThatResponse(
                            resp-> {
                                resp.statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
                                logger.info("código de respuesta: "+resp.extract().statusCode());
                            }
                    ));
        } catch (AssertionError e) {
            logger.warn("Error en la validación\n" + e);
            Assertions.fail(e.getMessage());
        }
    }
}
