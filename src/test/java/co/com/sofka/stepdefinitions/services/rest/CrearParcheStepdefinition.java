package co.com.sofka.stepdefinitions.services.rest;

import co.com.sofka.SetupRest;
import co.com.sofka.database.mongodb.MongodbCrud;
import co.com.sofka.model.services.rest.Parche;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Entonces;
import org.apache.http.HttpStatus;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;
import org.bson.Document;

import static co.com.sofka.database.mongodb.MongodbCrud.crearMongoCrud;
import static co.com.sofka.question.services.rest.FueCreadoParche.fueCreadoElParche;
import static co.com.sofka.task.services.rest.DoPost.doPost;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.equalTo;


public class CrearParcheStepdefinition extends SetupRest {
    private final Parche parche = Parche.generarParche();
    private static final Logger logger = LogManager.getLogger(CrearParcheStepdefinition.class);

    @Cuando("el usuario ejecute la peticion con datos validos para crear un parche")
    public void elUsuarioEjecuteLaPeticionConDatosValidosParaCrearUnParche() {
        try {
            super.setupRest();
            actor.attemptsTo(
                    doPost()
                            .withTheResource(CREAR_PARCHE)
                            .andTheBodyRequest(parche.parcheRequest())
            );
            logger.info("parche creado con exito");
        } catch (Exception e) {
            logger.warn("error al ejecutar petición\n"+e);
            Assertions.fail(e.getMessage());
        }
    }

    @Entonces("el parche debe ser almacenado en la base de datos")
    public void elParcheDebeSerAlmacenadoEnLaBaseDeDatos() {

        try {
            actor.should(
                    seeThatResponse(
                            resp-> {
                                resp.statusCode(HttpStatus.SC_OK);
                                logger.info("codigo de respuesta: "+resp.extract().statusCode());
                            }
                    ),
                    seeThat(
                            fueCreadoElParche(parche.parcheResponseMongodb(),
                                    consultarParcheCreado()),equalTo(true)
                    ));
            logger.info("parche encontrado en la base de datos");
        } catch (AssertionError e) {
            logger.warn("Error en la validación\n" + e);
            Assertions.fail(e.getMessage());
        }
    }
    protected String consultarParcheCreado(){

        MongodbCrud mongoDataBase =crearMongoCrud();

        Document doc = mongoDataBase.getDocumentColletion("parches",
                                                          parche.getDocumentParche());

        mongoDataBase.closeConnetions();
        return Parche.consultarParche(doc);
    }
}
