# language: es

Característica: Como usuario quiero acceder al Layout publico
  para visualizar las opciones de la pagina.

  Escenario: Verificar que el Boton Crear cuenta redireccione a la pagina correspondiente
    Dado que el usuario está en el Layout publico
    Cuando el usuario hace clic en el boton crear cuenta
    Entonces el usuario es redireccionado a la pagina correspondiente a crear cuenta

  Escenario: Verificar que el Boton inicio de sesion redireccione a la página correspondiente
    Dado que el cliente abrio la página del Layout publico
    Cuando el usuario hace clic en el boton inicio de sesion
    Entonces el usuario es redireccionado a la pagina correspondiente de inicio de sesion

  Escenario: Verificar que el Boton inicio redireccione a la pagina correspondiente
    Dado que el cliente accedio al sitio del Layout publico
    Cuando el cliente hace clic en el boton inicio
    Entonces el cliente es redireccionado a la pagina correspondiente de inicio