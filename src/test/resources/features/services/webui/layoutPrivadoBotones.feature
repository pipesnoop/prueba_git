# language: es

Característica: Como usuario quiero visualizar el layout privado
  para usar las diferentes opciones del sidebar.


  Escenario: Verificar el redireccionamiento del boton Mas en la Sidebar.
    Dado que el usuario se encuentra en el layout privado
    Cuando el usuario hace click en el boton Mas
    Entonces el usuario sera redireccionado a los servicios de la opcion del Mas "Fecha de Inicio:"

  Escenario: Verificar el redireccionamiento del boton Mis parches en la Sidebar.
    Dado que el cliente ha ingresado en el layout privado
    Cuando el cliente hace click en el boton Mis parches
    Entonces el cliente sera redireccionado a los servicios de la opcion Mis parches y vera "Encuentra tu parche ideal."

  Escenario: Verificar el redireccionamiento del boton Parches en la Sidebar.
    Dado que el consumidor ha ingresado en el layout privado
    Cuando el consumidor oprime el boton Parches
    Entonces el consumidor es redireccionado a los servicios de la opcion y vera "Busca un Parche"

  Escenario: Verificar el redireccionamiento del boton Perfil en la Sidebar.
    Dado que el cliente ha iniciado sesion en la pagina de el Parche
    Cuando el cliente hace click en el boton Perfil
    Entonces el cliente sera redireccionado a los servicios de la opcion Perfil y vera "Perfíl"

  Escenario: Verificar el redireccionamiento del botón inicio en la Sidebar.
    Dado que el consumidor ha realizado un inicio de sesion en la pagina de el Parche
    Cuando el consumidor hace click en el boton Inicio
    Entonces el cliente sera redireccionado a los servicios de la opcion Inicio y vera "Descripción"


  Escenario: Verificar el redireccionamiento del boton Cerrar sesion en la Sidebar.
    Dado que el usuario ha realizado un inicio de sesion en la pagina de el Parche
    Cuando el usuario hace click en el boton Cerrar sesion
    Entonces el usuario sera redireccionado a los servicios de la opcion Cerrar sesion y vera "Descripción"