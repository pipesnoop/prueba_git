#language: es
Característica: Vista de mi perfil
  Como usuario quiero ver mi información de perfil para conocer mis datos
  para cambiar mi nombre de usuario

  Antecedentes:
    Dado el usuario está registrado y se encuentra en la página de parches

  @UpgradeUser
  Escenario: Actualizacion exitosa
    Cuando el usuario envía la petición de actualizacion del usuario con los campos solicitados
    Entonces el usuario recibe un código de respuesta OK confirmando la actualizacion
    Y el usuario podra ver su nombre actualizado

  @UpgradeUser
  Escenario: Actualizacion fallida
    Cuando el usuario envía la petición de actualizacion del usuario con id inexistente
    Entonces el usuario recibe un código de respuesta Not Found