#language: es
Característica: Crear parches
  Yo como usuario autenticado
  quiero crear un parche
  para ofrecer un evento donde pueda visualizar la información y ubicación

  @crearParcheValido
  Escenario: crear parche con todos los campos validos
    Cuando el usuario ejecute la peticion con datos validos para crear un parche
    Entonces el parche debe ser almacenado en la base de datos

  @crearParcheNombreInvalido
  Escenario: crear parche con nombre invalido
    Cuando el usuario ejecute la peticion con nombre del parche "Rhoshandiatellyneshiaunneveshenk Koyaanisquatsiuth Williams"
    Entonces se debe lanzar un error por la cantida de caracteres en el nombre del parche

  @crearParcheConFechaDeFinInvalida
  Escenario: crear parche con fecha fin invalida
    Cuando el usuario ejecute la peticion con la fecha de fin mayor a la de inicio
    Entonces se debe lanzar un error por la fecha de fin incorrecta