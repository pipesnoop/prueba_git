#language: es
Característica: Eliminar parches
   Yo como usuario autenticado dueño de un parche
   quiero eliminarlo
   para que las personas no pierdan el tiempo yendo a ciertos lugares debido a cancelaciones o novedades


  @eliminarParche
  Escenario: eliminar parche con id
    Cuando el usuario ejecute la peticion para eliminar un parche
    Entonces el parche debe eliminarse de la base de datos