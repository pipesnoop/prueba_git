package co.com.sofka.question.services.rest.grupo4;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class ObtenerIdComentario implements Question<String> {

    public static Question<String> was(){
        return new ObtenerIdComentario();
    }

    @Override
    public String answeredBy(Actor actor) {
        return SerenityRest.lastResponse().jsonPath().getString("id");
    }
}
