package co.com.sofka.question.services.rest;

import co.com.sofka.util.services.rest.Estado;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class FueDesabilitadoParche implements Question<Boolean> {
   private String estadoParche;

    public FueDesabilitadoParche(String fueDesabilitadoParche) {
        this.estadoParche = fueDesabilitadoParche;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return estadoParche.equals(Estado.DESHABILITADO.name());
    }

    public static FueDesabilitadoParche seDesabilitoParche (String fueDesabilitadoParche) {
        return new FueDesabilitadoParche(fueDesabilitadoParche);
    }
}
