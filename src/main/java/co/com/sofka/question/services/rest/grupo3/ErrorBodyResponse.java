package co.com.sofka.question.services.rest.grupo3;

import co.com.sofka.model.services.rest.grupo3.ErrorModel;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class ErrorBodyResponse implements Question<ErrorModel> {
    @Override
    public ErrorModel answeredBy (Actor actor) {
        return SerenityRest.lastResponse().as(ErrorModel.class);
    }
}
