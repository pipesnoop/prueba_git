package co.com.sofka.question.ui;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterfaces.webui.LayoutPrivadoLocators.MESSAGE_VALIDATION_PARCHES;

public class LayoutPrivadoParchesQuestion implements Question <Object>{
    @Override
    public Object answeredBy(Actor actor) {
        return MESSAGE_VALIDATION_PARCHES.resolveFor(actor).getText();
    }
    public static LayoutPrivadoParchesQuestion layoutPrivadoParchesQuestion(){
        return new LayoutPrivadoParchesQuestion();
    }
}
