package co.com.sofka.question.ui;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterfaces.webui.LayoutPrivadoLocators.MESSAGE_VALIDATION_MI_PERFIL;

public class LayoutPrivadoMiPerfilQuestion implements Question <Object>{
    @Override
    public Object answeredBy(Actor actor) {
        return MESSAGE_VALIDATION_MI_PERFIL.resolveFor(actor).getText();
    }
    public static LayoutPrivadoMiPerfilQuestion layoutPrivadoMiPerfilQuestion(){
        return new LayoutPrivadoMiPerfilQuestion();
    }
}
