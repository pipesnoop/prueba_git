package co.com.sofka.question.ui;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterfaces.webui.LayoutPrivadoLocators.MESSAGE_VALIDATION_MAS;

public class LayoutPrivadoMasQuestion implements Question <Object>{
    @Override
    public Object answeredBy(Actor actor) {
        return MESSAGE_VALIDATION_MAS.resolveFor(actor).getText();
    }
    public static LayoutPrivadoMasQuestion layoutPrivadoMasQuestion(){
        return new LayoutPrivadoMasQuestion();
    }
}
