package co.com.sofka.model.services.rest.grupo4;

public class UpgradeUserModel  {

   private String id;
   private String uid;
   private String nombres;
   private String email;
   private String imagenUrl;

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public String getUid() {
      return uid;
   }

   public void setUid(String uid) {
      this.uid = uid;
   }

   public String getNombres() {
      return nombres;
   }

   public void setNombres(String nombres) {
      this.nombres = nombres;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getImagenUrl() {
      return imagenUrl;
   }

   public void setImagenUrl(String imagenUrl) {
      this.imagenUrl = imagenUrl;
   }
}
