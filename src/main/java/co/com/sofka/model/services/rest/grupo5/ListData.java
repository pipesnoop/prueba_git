package co.com.sofka.model.services.rest.grupo5;

public class ListData {

    private String id;
    private String duenoDelParche;
    private String nombreParche;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDuenoDelParche() {
        return duenoDelParche;
    }

    public void setDuenoDelParche(String duenoDelParche) {
        this.duenoDelParche = duenoDelParche;
    }

    public String getNombreParche() {
        return nombreParche;
    }

    public void setNombreParche(String nombreParche) {
        this.nombreParche = nombreParche;
    }
}
