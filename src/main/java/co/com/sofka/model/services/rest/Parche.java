package co.com.sofka.model.services.rest;

import co.com.sofka.util.services.rest.Estado;
import co.com.sofka.util.services.rest.Ubicacion;
import com.github.javafaker.Faker;
import org.bson.Document;

import java.time.LocalDateTime;
import static co.com.sofka.util.MapaStrings.*;
import static co.com.sofka.util.services.rest.Categoria.seleccionarCategoria;
import static co.com.sofka.util.services.rest.Ubicacion.generarUbicacion;

public class Parche {
    private String dueno;
    private String nombre;
    private String descripcion;
    private String fechaInicio;
    private String fechaFin;
    private String estado;
    private String categoria;
    private int capacidadMaxima;
    private Ubicacion ubicacion;
    private static final String DUENO_PARCHE = "grupo2";

    public void setDueno(String dueno) {
        this.dueno = dueno;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setCapacidadMaxima(int capacidadMaxima) {
        this.capacidadMaxima = capacidadMaxima;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }


    public String getFechaInicio() {
        return fechaInicio;
    }

    public String parcheRequest() {
        return "{" +'\n' +
                "  \"duenoDelParche\":\"" + dueno + COMILLA_COMA.getValue() +
                "  \"nombreParche\":\"" + nombre + COMILLA_COMA.getValue() +
                "  \"descripcion\":\"" + descripcion + COMILLA_COMA.getValue() +
                "  \"fechaInicio\":\"" + fechaInicio + COMILLA_COMA.getValue() +
                "  \"fechaFin\":\"" + fechaFin + COMILLA_COMA.getValue() +
                "  \"estado\":\"" + estado + COMILLA_COMA.getValue() +
                "  \"categoria\":\"" + categoria + COMILLA_COMA.getValue() +
                "  \"capacidadMaxima\":" + capacidadMaxima + COMA_SALTO_LINEA.getValue() +
                "  \"ubicacionParche\":" + ubicacion + SALTO_LINEA.getValue() +
                '}';
    }
    public  String parcheResponseMongodb(){
        return "{" +'\n' +
                "  \"duenoDelParche\":\"" + dueno + COMILLA_COMA.getValue() +
                "  \"nombreParche\":{" +"\"valorNombre\":\""+ nombre + COMILLA_COMA_LLAVE.getValue() +
                "  \"descripcion\":{" +"\"valorDescripcion\":\"" + descripcion + COMILLA_COMA_LLAVE.getValue() +
                "  \"estado\":\"" + estado + COMILLA_COMA.getValue() +
                "  \"categoria\":\"" + categoria + COMILLA_COMA.getValue() +
                "  \"ubicacion\":" + ubicacion + SALTO_LINEA.getValue() +
                '}';
    }
    public String parcheRequestEditar(){
        return "{"+
        "\"id\":" + "\"%s\"" +","+
                "\"duenoDelParche\":\""+dueno+"\","+
                "\"nombreParche\":\""+nombre+"\","+
                "\"descripcion\":\""+descripcion+"\","+
                "\"fechaCreacion\":\""+ "2022-12-11T10:59:11.332"+"\","+
                "\"fechaInicio\":"+ "\"2022-12-12T10:59:11.332\""+","+
                "\"fechaFin\":"+ "\"2022-12-12T12:59:11.332\""+","+
                "\"estado\":"+ "\"HABILITADO\""+","+
                "\"categoria\":\""+categoria+"\","+
                "\"capacidadMaxima\":"+ capacidadMaxima+","+
                "\"ubicacionParche\":"+"{"+
            "\"lat\":"+ ubicacion.getLat()+","+
                    "\"lng\":"+ ubicacion.getLng()+","+
                    "\"formatted\":"+ "\"CIUDAD DE COLOMBIA\""+
        "},"+
        "\"cantidadParticipantes\":"+ "2"+
    "}";
    }
    public Document getDocumentParche (){
        Document parche = new Document();
        parche.append("duenoDelParche",dueno);

        Document nombreParche = new Document();
        nombreParche.append("valorNombre",nombre);
        parche.append("nombreParche",nombreParche);


        Document descripcionParche = new Document();
        descripcionParche.append("valorDescripcion",descripcion);
        parche.append("descripcion",descripcionParche);

        parche.append("estado",estado);
        parche.append("categoria",categoria);

        Document ubicacionParche = new Document();
        ubicacionParche.append("lat",this.ubicacion.getLat());
        ubicacionParche.append("lng",this.ubicacion.getLng());

        parche.append("ubicacion",ubicacionParche);
        return parche;
    }
    public static Parche generarParche(){
        Faker javaFaker = new Faker();
        Parche parche = new Parche();

        parche.setDueno(Parche.DUENO_PARCHE);
        parche.setNombre(javaFaker.team().name());
        parche.setDescripcion(javaFaker.lorem().paragraph());

        parche.setFechaInicio(LocalDateTime.now().plusDays(2).toString());
        parche.setFechaFin(LocalDateTime.parse(parche.getFechaInicio()).plusDays(2).toString());

        parche.setEstado(Estado.HABILITADO.name());
        parche.setCategoria(seleccionarCategoria());
        parche.setCapacidadMaxima(javaFaker.number().numberBetween(1,500));
        parche.setUbicacion(generarUbicacion());

        return parche;
    }
    public static String consultarParche(Document doc){

        Parche parcheBuscado = new Parche();
        parcheBuscado.setDueno(doc.getString("duenoDelParche"));

        Document docNombre = (Document) doc.get("nombreParche");
        parcheBuscado.setNombre(docNombre.getString("valorNombre"));

        Document docDescripcion = (Document) doc.get("descripcion");
        parcheBuscado.setDescripcion(docDescripcion.getString("valorDescripcion"));

        Document docFechaInicio = (Document) doc.get("fechaInicio");
        parcheBuscado.setFechaInicio(docFechaInicio.getDate("valorFecha").toString());

        Document docFechaFin = (Document) doc.get("fechaFin");
        parcheBuscado.setFechaFin(docFechaFin.getDate("valorFecha").toString());

        parcheBuscado.setEstado(doc.getString("estado"));
        parcheBuscado.setCategoria(doc.getString("categoria"));

        Document docCapacidad = (Document) doc.get("capacidadMaxima");
        parcheBuscado.setCapacidadMaxima(Math.toIntExact(docCapacidad.getLong("valorCapacidad")));

        Ubicacion ubicacionParcheBuscado = new Ubicacion();
        Document docUbicacion = (Document) doc.get("ubicacion");
        ubicacionParcheBuscado.setLat(docUbicacion.getDouble("lat"));
        ubicacionParcheBuscado.setLng(docUbicacion.getDouble("lng"));
        parcheBuscado.setUbicacion(ubicacionParcheBuscado);

        return parcheBuscado.parcheResponseMongodb();
    }
}
