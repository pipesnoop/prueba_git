package co.com.sofka.task.services.rest;

import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Post;


public class DoPost implements Task {

    private String resource;
    private Object bodyRequest;

    public DoPost withTheResource(String resource) {
        this.resource = resource;
        return this;
    }

    public DoPost andTheBodyRequest(Object bodyRequest) {
        this.bodyRequest = bodyRequest;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to(resource)
                        .with(
                                requestSpecification -> requestSpecification.contentType(ContentType.JSON).relaxedHTTPSValidation()
                                        .body(bodyRequest)
                        )
        );


    }

    public static DoPost doPost(){
        return new DoPost();
    }

}
