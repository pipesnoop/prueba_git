package co.com.sofka.task.services.rest;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Delete;

import java.util.Map;

public class DoDeleteParche implements Task {
    private String resource;
    private Map<String, Object> headers;
    private String bodyRequest;

    public DoDeleteParche withTheResource(String resource) {
        this.resource = resource;
        return this;
    }

    public DoDeleteParche andTheHeaders(Map<String, Object> headers) {
        this.headers = headers;
        return this;
    }

    public DoDeleteParche andTheBodyRequest(String bodyRequest) {
        this.bodyRequest = bodyRequest;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
               Delete.from (resource)
                     .with(
                             requestSpecification -> requestSpecification.relaxedHTTPSValidation()

                                        .body(bodyRequest)
                     )
        );
    }
    public static DoDeleteParche doDeleteParche(){
        return new DoDeleteParche();
    }
}
