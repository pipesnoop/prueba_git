package co.com.sofka.task.ui.layout;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.sofka.userinterfaces.webui.LayoutPrivadoLocators.PARCHES;

public class LayoutPrivadoParches implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(PARCHES)
        );
    }

    public static LayoutPrivadoParches layoutPrivadoParches(){
        return new LayoutPrivadoParches();
    }
}
