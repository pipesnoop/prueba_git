package co.com.sofka.task.ui.layout;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.sofka.userinterfaces.webui.LayoutPrivadoLocators.INICIO_PRIVADO;

public class LayoutPrivadoInicio implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(INICIO_PRIVADO)
        );
    }

    public static LayoutPrivadoInicio layoutPrivadoInicio(){
        return new LayoutPrivadoInicio();
    }
}
