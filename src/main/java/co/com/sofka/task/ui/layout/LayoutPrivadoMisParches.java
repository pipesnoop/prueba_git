package co.com.sofka.task.ui.layout;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.sofka.userinterfaces.webui.LayoutPrivadoLocators.MIS_PARCHES;

public class LayoutPrivadoMisParches implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(MIS_PARCHES)
        );
    }

    public static LayoutPrivadoMisParches layoutPrivadoMisParches(){
        return new LayoutPrivadoMisParches();
    }
}
