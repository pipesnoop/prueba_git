package co.com.sofka.task.ui.layout;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.sofka.userinterfaces.webui.LayoutPublicLocatos.CREAR_CUENTA;


public class LayoutPublicoCrearCuenta implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(CREAR_CUENTA)
        );
    }

    public static LayoutPublicoCrearCuenta layoutPublicoCrearCuenta(){
        return new LayoutPublicoCrearCuenta();
    }
}
