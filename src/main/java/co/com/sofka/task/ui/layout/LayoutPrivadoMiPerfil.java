package co.com.sofka.task.ui.layout;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.sofka.userinterfaces.webui.LayoutPrivadoLocators.MI_PERFIL;

public class LayoutPrivadoMiPerfil implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(MI_PERFIL)
        );
    }

    public static LayoutPrivadoMiPerfil layoutPrivadoMiPerfil(){
        return new LayoutPrivadoMiPerfil();
    }
}
