package co.com.sofka.task.ui.layout;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.sofka.userinterfaces.webui.LayoutPublicLocatos.INICIO_SESION;


public class LayoutPublicoInicioDeSesion implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(INICIO_SESION)
        );
    }

    public static LayoutPublicoInicioDeSesion layoutPublicInicioDeSesion(){
        return new LayoutPublicoInicioDeSesion();
    }
}
