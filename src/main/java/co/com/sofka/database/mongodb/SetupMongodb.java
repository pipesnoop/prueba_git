package co.com.sofka.database.mongodb;

import com.mongodb.ConnectionString;

import com.mongodb.MongoClientSettings;
import com.mongodb.MongoException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;


public class SetupMongodb {
    private static final Logger logger = LogManager.getLogger(SetupMongodb.class);
    protected MongoClient mongoClient;
    protected MongoDatabase database;

    public SetupMongodb(String hostName, String dataBaseName) throws MongoException {
        ConnectionString connectionString = new ConnectionString(hostName);
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .build();
        mongoClient = MongoClients.create(settings);
        this.database= mongoClient.getDatabase(dataBaseName);

    }
    public void closeConnetions(){
        mongoClient.close();
    }

    public static Properties configMongoDB(){

        Properties configMongo = new Properties();

        try {
            try (FileReader fileReader = new FileReader("mongodb.properties")) {
                configMongo.load(fileReader);
                return configMongo;
            }
        } catch (IOException e) {
            logger.warn("Error al acceder\n",e );
        }
        return configMongo;
    }
}
