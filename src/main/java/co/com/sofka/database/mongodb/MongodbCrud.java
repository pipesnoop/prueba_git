package co.com.sofka.database.mongodb;

import com.mongodb.MongoException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import org.bson.Document;


public class MongodbCrud extends SetupMongodb {

    public MongodbCrud(String hostName, String dataBaseName) throws MongoException {
        super(hostName, dataBaseName);
    }

    public MongoCollection<Document> getAllDocument(String collectionName) {
      return database.getCollection(collectionName);
    }

    public  Document getDocumentColletion(String collectionName, Document filter ){
        FindIterable<Document> respuestaFiltro = database.getCollection(collectionName).find().filter(filter);
        return respuestaFiltro.first();
    }
    public  Document getDocumentColletion(String collectionName){
        FindIterable<Document> respuestaFiltro = database.getCollection(collectionName).find();
        return respuestaFiltro.first();
    }

    public static MongodbCrud crearMongoCrud(){

        return new MongodbCrud(
                SetupMongodb.configMongoDB().getProperty("hostName.parche"),
                SetupMongodb.configMongoDB().getProperty("dataBaseName"));
    }
}
