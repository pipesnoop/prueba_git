package co.com.sofka.userinterfaces.webui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.xpath;

public class LayoutPublicLocatos extends PageObject {

    public static final Target INICIO = Target
            .the("Botón Inicio")
            .located(id("botonInicioNavBar"));

    public static final Target INICIO_SESION = Target
            .the("Boton Inicio de Sesión")
            .located(id("botonInicioSesionNavBar"));

    public static final Target CREAR_CUENTA = Target
            .the("Boton Crear Cuenta")
            .located(id("botonCrearCuentaNavBar"));



    //For validations

    public static final Target MESSAGE_VALIDATION_INICIO = Target
            .the("Validacion de inicio")
            .located(xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/h2[2]"));

    public static final Target MESSAGE_VALIDATION_INICIO_SESION = Target
            .the("Validacion inicio de sesion")
            .located(xpath("//*[@id=\"root\"]/div/div/main/div[1]/h2"));

    public static final Target MESSAGE_VALIDATION_CREAR_CUENTA = Target
            .the("Validacion crear cuenta")
            .located(xpath("//*[@id=\"root\"]/div/div/main/div/h2"));
}
