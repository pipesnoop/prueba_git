package co.com.sofka.userinterfaces.webui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

import static org.openqa.selenium.By.*;

public class LayoutPrivadoLocators extends PageObject {

    public static final Target CREAR_CUENTA_PRIVADO = Target
            .the("Boton Crear Cuenta")
            .located(id("botonCrearCuentaNavBar"));

    public static final Target NAME_USER = Target
            .the("nombre del usuario")
            .located(id("nombreIngreso"));

    public static final Target EMAIL_USER = Target
            .the("Correo de usuario")
            .located(id("email"));

    public static final Target PASSWORD_USER = Target
            .the("Password de usuario 1")
            .located(id("password"));

    public static final Target PASSWORD_USER_2 = Target
            .the("Password de usuario 2")
            .located(id("confPassword"));

    public static final Target BOTON_CREAR_CUENTA = Target
            .the("Password de google")
            .located(xpath("//*[@id=\"root\"]/div/div/main/div/form/button[1]"));

    public static final Target INICIO_PRIVADO = Target
            .the("Botón para ir al Inicio")
            .located(xpath("//*[@id=\"root\"]/div/div/div[1]/div[1]/div/ul/li[1]/a/div"));

    public static final Target MAS = Target
            .the("Boton MAS")
            .located(xpath("//*[@id=\"root\"]/div/div/div[1]/div[1]/div/div[2]/div/button"));

    public static final Target MI_PERFIL = Target
            .the("Botón para ir a mi perfil")
            .located(cssSelector("i[class='fas fa-user-cog fa-lg']"));

    public static final Target MIS_PARCHES = Target
            .the("Botón para ir a mis parches")
            .located(cssSelector("i[class='fas fa-people-arrows fa-lg']"));

    public static final Target PARCHES = Target
            .the("Botón para ir a todos los parches")
            .located(cssSelector("i[class='fas fa-users fa-lg']"));

    public static final Target CERRAR_SESION = Target
            .the("Botón para cerrar sesion")
            .located(cssSelector("span[class='text-sm self-center ml-2 font-semibold']"));

    //For validations

    public static final Target MESSAGE_VALIDATION_MAS = Target
            .the("Validacion de boton MAS")
            .located(cssSelector("label[class=text-gray-600]"));

    public static final Target MESSAGE_VALIDATION_MI_PERFIL = Target
            .the("Validacion de mi perfil")
            .located(xpath("//*[@id=\"root\"]/div/div/div[2]/div/div/div[1]/div[1]/h3"));

    public static final Target MESSAGE_VALIDATION_MIS_PARCHES = Target
            .the("Validacion de mis parches")
            .located(id("texto-titulo-filtro"));

    public static final Target MESSAGE_VALIDATION_PARCHES = Target
            .the("Validacion de todos los parches")
            .located(id("texto-titulo-filtro"));

    public static final Target MESSAGE_VALIDATION_CERRAR_SESION = Target
            .the("Validacion de cerrar sesion")
            .located(xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/h2[2]"));

    public static final Target MESSAGE_VALIDATION_INICIO_PRIVADO = Target
            .the("Botón para ir al Inicio privado")
            .located(xpath("//*[@id=\"root\"]/div/div/div[2]/div/div/div[1]/div[3]/h2[2]"));

}
