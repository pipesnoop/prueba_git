package co.com.sofka.util.services.rest;

import com.github.javafaker.Faker;

public class Ubicacion {
    private double lat;
    private double lng;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "{" +
                "\"lat\":" + lat +
                ", \"lng\":" + lng +
                '}';
    }

    public static Ubicacion generarUbicacion(){
        Ubicacion ubicacion = new Ubicacion();
        Faker faker = new Faker();
        ubicacion.setLat(faker.number().randomDouble(2,1,500));
        ubicacion.setLng(faker.number().randomDouble(2,1,500));
        return ubicacion;
    }
}
